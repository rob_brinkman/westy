(function () {
  'use strict';

  angular
    .module('frontend')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController() {
    var main = this;

    main.sensor = [
      {
        topic: '/sensor/oil/temperature',
        title: 'Oil Temperature',
        type: 'temperature'
      }, {
        topic: '/sensor/climate/inside/temperature',
        title: 'Inside Temperature',
        type: 'temperature'
      }, {
        topic: '/sensor/climate/inside/humidity',
        title: 'Inside Humidity',
        type: 'humidity'
      },
      {
        topic: '/sensor/position/tilt',
        title: 'Westy Tilt',
        type: 'text'
      },
      {
        topic: '/sensor/climate/outside/temperature',
        title: 'Outside Temperature',
        type: 'temperature'
      }, {
        topic: '/sensor/climate/outside/humidity',
        title: 'Outside Humidity',
        type: 'humidity'
      }, {
        topic: '/sensor/water/clean/level',
        title: 'Clean Water Level',
        type: 'level'
      }, {
        topic: '/sensor/power/main/current',
        title: 'Main Power Current',
        type: 'current'
      }, {
        topic: '/sensor/power/main/voltage',
        title: 'Main Power Voltage',
        type: 'voltage'
      }, {
        topic: '/sensor/power/secondary/current',
        title: 'Secondary Power Current',
        type: 'current'
      }, {
        topic: '/sensor/power/secondary/voltage',
        title: 'Secondary Power Voltage',
        type: 'voltage'
      }];
  }
})();
