(function () {
  'use strict';

  angular
    .module('frontend', ['ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'restangular',
      'ui.router',
      'ui.bootstrap',
      'knalli.angular-vertxbus',
      'angularMoment',
      'nvd3',
      'webcam'
    ]).
    config(function (vertxEventBusProvider) {

      vertxEventBusProvider
        .enable()
        .useReconnect()
        .useDebug(true)
        // .useUrlServer('http://10.0.0.1:8080');
        .useUrlServer('http://192.168.1.74:8080');
        //.useUrlServer('http://localhost:8080');
    });
  ;

})();
