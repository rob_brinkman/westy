(function () {
  'use strict';

  angular
    .module('frontend')
    .directive('sensor', sensor);


  /** @ngInject */
  function sensor() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/sensor/sensor.html',
      bindToController: true,
      controller: SensorController,
      controllerAs: 'sensor',
      scope: {
        topic: '@',
        title: '@',
        item: '@',
        type: '@'
      }
    };

    return directive;

    /** @ngInject */
    function SensorController(vertxEventBusService) {

      var sensor = this;

      var formatter = formatters[sensor.type] || formatters['default'];
      sensor.value = formatter(0);

      sensor.graphConfig = {
        deepWatchData: true,
      }

      sensor.graphOptions = {
        chart: {
          type: 'lineChart',
          height: 200,
          margin: {
            top: 10,
            right: 40,
            bottom: 20,
            left: 50
          },
          duration: 50,
          showLegend: false,
          xAxis: {
            axisLabel: '',
            showMaxMin: true,
            tickFormat: function (d) {
              return d3.time.format('%H:%M:%S')(new Date(d));
            }
          },
          yAxis: {
            axisLabel: '',
            showMaxMin: false,
            tickFormat: function (d) {
              return formatter(d);
            },
            rotateYLabel: false
          }
        }
      };

      sensor.graphData = [{
        key: sensor.title,
        values: [],
        color: sensor.color || '#5cb85c'
      }];

      function addGraphData(message) {

        sensor.lastReceived = message.received;
        sensor.value = formatter(message.payload);
        if (sensor.type !== 'text') {
          sensor.graphData[0].values.push({
            x: sensor.lastReceived,
            y: message.payload
          });
        }
      }


      vertxEventBusService.on(getAddressForTopic(sensor.topic), function (message) {
        addGraphData(message);
      });

      // load initialData
      function loadIinitialData() {
        vertxEventBusService.send('westy.base.mqtt.request', {topic: sensor.topic, history: 3600})
          .then(function (resp) {
            _.each(resp.result, addGraphData);
          }).catch(function (err) {
            console.log("Error: " + err);
            toastr.error("Could not retrieve history: " + err);
          });
      }
      loadIinitialData();
    }
  }


  var formatters = {
    'default': function (v) {
      return v;
    },
    'temperature': function (v) {
      return d3.format(',.0f')(v) + " \u00B0C"
    },
    'humidity': function (v) {
      return d3.format(',.0f')(v) + " %"
    },
    'current': function (v) {
      return d3.format(',.2f')(v) + " A"
    },
    'voltage': function (v) {
      return d3.format(',.2f')(v) + " V"
    }
  }

  function getAddressForTopic(topic) {
    return 'westy.base.mqtt' + topic.replace(/\//g, '.');
  }
})();
