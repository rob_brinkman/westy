(function() {
  'use strict';

  angular
    .module('frontend')
    .directive('navbar', navbar);

  /** @ngInject */
  function navbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
      },
      controller: NavbarController,
      controllerAs: 'navbar',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController($rootScope, toastr, $window) {
      var vm = this;

      vm.connected = false;

      $rootScope.$on('vertx-eventbus.system.connected', function() {
        toastr.success("Connected to BaseStation");
        vm.connected = true;
      });
      $rootScope.$on('vertx-eventbus.system.disconnected', function() {
        toastr.warning("Disconnected from BaseStation");
        vm.connected = false;
      });

     vm.reloadPage = function () {
       $window.location.reload();
     }
    }
  }
})();
