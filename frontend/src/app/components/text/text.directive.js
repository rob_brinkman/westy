(function () {
  'use strict';

  angular
    .module('frontend')
    .directive('text', text);


  /** @ngInject */
  function text() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/text/text.html',
      bindToController: true,
      controller: TextController,
      controllerAs: 'text',
      scope: {
        topic: '@',
        title: '@'
      }
    };

    return directive;

    /** @ngInject */
    function TextController(vertxEventBusService) {

      var text = this;

      console.dir(text.topic);
      vertxEventBusService.on(getAddressForTopic(text.topic), function (message) {
        text.value = message.payload;
        text.lastReceived = message.received;
      });
    }
  }


  function getAddressForTopic(topic) {
    return 'westy.base.mqtt' + topic.replace(/\//g, '.');
  }
})();
