#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHT.h"

/************************* WiFi Access Point *********************************/

#define WLAN_SSID       "Westy"
#define WLAN_PASS       "Westy1982"

/************************* MQTT Setup *********************************/

#define MQTT_CLIENTID    "WestyPower"
#define MQTT_SERVER      "10.0.0.1"
#define MQTT_SERVERPORT  8883

WiFiClient wifiClient;
PubSubClient client(wifiClient);

void setup() {
  initSerial();
}

void loop() {
  connectWifi();
  connectMQTT();

  
  delay(60000);
  }

void initSerial() {
  Serial.begin(115200);
  delay(10);
  Serial.println();
  Serial.println();
}

void connectWifi() {
  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("(Re)connecting to ");
    Serial.println(WLAN_SSID);

    WiFi.begin(WLAN_SSID, WLAN_PASS);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println();
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
}

void connectMQTT() {

  if (!client.connected()) {
    client.setServer(MQTT_SERVER, MQTT_SERVERPORT);
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect and set our last will
    if (client.connect(MQTT_CLIENTID, "/westy/power/status", 1, false, "disconnected")) {
      Serial.println("connected");
      client.publish("/westy/power/status/status", "connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  } else {
    // This should be called regularly to allow the client to process incoming messages and maintain its connection to the server.
    client.loop();
  }
}


