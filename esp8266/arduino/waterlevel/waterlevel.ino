#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHT.h"

/************************* WiFi Access Point *********************************/

#define WLAN_SSID       "Westy"
#define WLAN_PASS       "Westy1982"

/************************* MQTT Setup *********************************/

#define MQTT_CLIENTID    "WestyWaterLevel"
#define MQTT_SERVER      "10.0.0.1"
#define MQTT_SERVERPORT  8883

/************************* Level Pin Setup *********************************/
#define LEVEL_FULL_PIN    4
#define LEVEL_HALF_PIN    5
#define LEVEL_EMPTY_PIN   14
#define DHTTYPE           DHT11


/************************* Loop Setup *********************************/
#define MAINLOOPDURATION   29500
#define CHECKLOOPDURATION  500

WiFiClient wifiClient;
PubSubClient client(wifiClient);

int loopCount = 0;
int level = 0;
boolean levelChangeDetected = true;


void setup() {
  initSerial();
  initLevelSensor();
}

void loop() {
  loopCount++;

  if (loopCount % (MAINLOOPDURATION / CHECKLOOPDURATION) == 0) {
    loopCount = 0;
    // Every 30 seconds
    connectWifi();
    connectMQTT();
  }
  if (levelChangeDetected) {
    levelChangeDetected = false;
    client.publish("/sensor/water/clean/level", numberToCharArray(level));
  }
  delay(CHECKLOOPDURATION);
}

void initSerial() {
  Serial.begin(115200);
  delay(10);
  Serial.println();
  Serial.println();
}


void initLevelSensor() {
  pinMode(LEVEL_FULL_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(LEVEL_FULL_PIN), levelChanged, CHANGE);
  pinMode(LEVEL_HALF_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(LEVEL_HALF_PIN), levelChanged, CHANGE);
  pinMode(LEVEL_EMPTY_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(LEVEL_EMPTY_PIN), levelChanged, CHANGE);
}



void levelChanged() {
  int levelFull = digitalRead(LEVEL_FULL_PIN);
  int levelHalf = digitalRead(LEVEL_HALF_PIN);
  int levelEmpty = digitalRead(LEVEL_EMPTY_PIN);

  if (levelFull) {
    level = 3;
  } else if (levelHalf) {
    level = 2;
  } else if (levelEmpty) {
    level = 1;
  } else {
    level = 0;
  }
  
  Serial.print("Level: ");
  Serial.println(level);
  
  levelChangeDetected = true;
}

void connectWifi() {
  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("(Re)connecting to ");
    Serial.println(WLAN_SSID);

    WiFi.begin(WLAN_SSID, WLAN_PASS);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println();
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  yield();
}

void connectMQTT() {
  if (!client.connected()) {
    client.setServer(MQTT_SERVER, MQTT_SERVERPORT);
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect and set our last will
    if (client.connect(MQTT_CLIENTID, "/westy/climate/inside/status", 1, false, "disconnected")) {
      Serial.println("connected");
      client.publish("/westy/climate/inside/status", "connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 1 seconds");
      delay(1000);
    }
  }
  yield();
}


char* numberToCharArray(float value) {
  static char dtostrfbuffer[15];
  dtostrf(value, 1, 2, dtostrfbuffer);
  return dtostrfbuffer;
}
