#include <MQTT.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHT.h"

/************************* WiFi Access Point *********************************/

#define WLAN_SSID       "Westy"
#define WLAN_PASS       "Westy1982"

/************************* MQTT Setup *********************************/

#define MQTT_CLIENTID    "WestyClimate"
#define MQTT_SERVER      "10.0.0.1"
#define MQTT_SERVERPORT  8883

/************************* Temperature Sensor Setup *********************************/
#define DHTPIN1           6
#define DHTTYPE           DHT11

/************************* Tilt Sensor Setup *********************************/
#define TILTPIN1           7
#define TILTPIN2           8

/************************* Loop Setup *********************************/
#define MAINLOOPDURATION   29500
#define CHECKLOOPDURATION  500


char const * tiltDirections[4] = {"front", "left", "right", "rear"};
int tiltStatus = 0;
boolean tiltStatusChanged = false;

int loopCount = 0;

IPAddress server(10, 0, 0, 1);
WiFiClient wifiClient;
PubSubClient client(server);
DHT dhtInside(DHTPIN1, DHTTYPE);

// Enable battery monitoring
ADC_MODE(ADC_VCC);

void setup() {
  initSerial();
  //initDHT();
  //initLevelSensor();
}

void loop() {
  loopCount++;

  if (loopCount % (MAINLOOPDURATION / CHECKLOOPDURATION) == 0) {
    loopCount = 0;
    // Every 30 seconds

    connectWifi();
    connectMQTT();

    //client.publish("/sensor/climate/inside/temperature", readTemperature(dhtInside));
    //yield();
    //client.publish("/sensor/climate/inside/humidity",   readHumidity(dhtInside));
    //yield();
    //client.publish("/sensor/climate/inside/batterylevel", readBatteryLevel());
    //yield();
  }

  if (tiltStatusChanged) {
    client.publish("/westy/tilt", tiltDirections[tiltStatus]);
    Serial.println( tiltDirections[tiltStatus]);
    tiltStatusChanged = false;
  }

  delay(CHECKLOOPDURATION);

  // Delay for 5 seconds to allow sending of message
  //delay(5000);

  // Deepsleep for 25 seconds
  // ESP.deepSleep(25 * 1000000, WAKE_RF_DEFAULT);
}

void initSerial() {
  Serial.begin(115200);
  delay(10);
  Serial.println();
  Serial.println();
  Serial.println("AA");
}

void initDHT() {
  dhtInside.begin();
}

void initLevelSensor() {
  pinMode(TILTPIN1, INPUT);
  attachInterrupt(digitalPinToInterrupt(TILTPIN1), tiltChanged, CHANGE);
  pinMode(TILTPIN2, INPUT);
  attachInterrupt(digitalPinToInterrupt(TILTPIN2), tiltChanged, CHANGE);
}



void tiltChanged() {
  int s1 = digitalRead(TILTPIN1);
  int s2 = digitalRead(TILTPIN2);

  int status = (s1 << 1) | s2;
  if (status != tiltStatus) {
    tiltStatus = status;
    tiltStatusChanged = true;
  }
}

void connectWifi() {
  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("(Re)connecting to ");
    Serial.println(WLAN_SSID);

    WiFi.begin(WLAN_SSID, WLAN_PASS);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println();
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  yield();
}

void connectMQTT() {
  if (!client.connected()) {
  
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect and set our last will
    if (client.connect(MQTT_CLIENTID, "/westy/climate/inside/status", 1, false, "disconnected")) {
      Serial.println("connected");
      client.publish("/westy/climate/inside/status", "connected");
    } else {
      Serial.print("failed, rc=");
    //  Serial.print(client.state());
      Serial.println(" try again in 1 seconds");
      delay(1000);
    }
  }
  yield();
}

char* readTemperature(DHT dht) {
  return floatToCharArray(dht.readTemperature());
}

char* readHumidity(DHT dht) {
  return floatToCharArray(dht.readHumidity());
}

char* readBatteryLevel() {
  return floatToCharArray(ESP.getVcc());
}

char* floatToCharArray(float value) {
  static char dtostrfbuffer[15];
  dtostrf(value, 1, 2, dtostrfbuffer);
  return dtostrfbuffer;
}
