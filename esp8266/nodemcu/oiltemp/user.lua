mqttConnected = false
outputPin = 2 

function loadConfiguration(configFile) 
    if file.open(configFile) then
        file.close() 
        dofile(configFile)
    else
        error(configFile .. " not found!!!!")
    end
end

function connectWifi (callback)  
    wifi.setmode(wifi.STATION)
    wifi.sleeptype(wifi.NONE_SLEEP)
    wifi.sta.config(wifi_ssid,wifi_password, 1) 
    wifi.sta.eventMonReg(wifi.STA_IDLE, function() print("Wifi Idle") end)
    wifi.sta.eventMonReg(wifi.STA_CONNECTING, function() print("Wifi Connecting") end)
    wifi.sta.eventMonReg(wifi.STA_WRONGPWD, function() print("Wifi Wrong Password") end)
    wifi.sta.eventMonReg(wifi.STA_APNOTFOUND, function() print("Wifi App Not Found") end)
    wifi.sta.eventMonReg(wifi.STA_FAIL, function() print("Wifi Connection Failed") end)
    wifi.sta.eventMonReg(wifi.STA_GOTIP, function() 
        print("Wifi Connected")
        callback() 
    end)
    wifi.sta.eventMonStart()
    wifi.sta.connect()
end

function connectMQTT() 
    -- init mqtt client with keepalive timer 120sec
    m = mqtt.Client(mqtt_clientid, 120, nil, nil)
    
    -- register event handlers
    m:on("connect", function(client) 
        mqttConnected = true
        print ("MQTT Connected") 
    end)

    m:on("offline", function(client) 
        mqttConnected = false
        print ("MQTT Disconnected")
    end)
       
    -- connect to broker
    result = m:connect(mqtt_host, mqtt_port, 0, 1)
end

function enableAdcReader() 
    tmr.alarm(3, 5000, 1, function()
        gpio.write(outputPin, gpio.HIGH);
        local vOut = adc.read(0) 
        if (vOut and mqttConnected) then
            local resistance = vOutToResistance(vOut)
            local temp = resistanceToTemperature(resistance)
            print("Publish Resistance: " .. resistance .. ", temperature: " .. temp)
            m:publish( "/sensor/oil/temperature", tostring(temp), 0, 0, nil)
        end  
        gpio.write(outputPin, gpio.LOW);
    end)    
end

function vOutToResistance(vOut) 
    local r1 = 2200     --  2.2 K ohm
    local vIn  = 3300   --  3.3 Volt
    return r1 * (vOut/(vIn-vOut))
end

-- Based on the Steinhart-Hart equation
-- See http://www.useasydocs.com/theory/ntc.htm
function resistanceToTemperature(r) 
    local a = 0.0015720325168209438
    local b = 0.0002731146044737263
    local c = -2.8675045223564176e-7
    local rLog = loge(r);
    return (1 / (a + (b * rLog) + (c* rLog * rLog * rLog))) - 273.15;
end

function loge (z)  
    local sum=0
    local resolution = 100
    for n=0,resolution do
        sum = sum + (1/(2*n+1))*((z-1)/(z+1))^(2*n+1)
    end
    return 2*sum
end


function initOutputPin() 
    gpio.mode(outputPin, gpio.OUTPUT)
    gpio.write(outputPin, gpio.LOW);
end


loadConfiguration("settings.lua")

-- mute serial to improve stability
node.output(function() end, 0)

initOutputPin()

connectWifi(function()
    connectMQTT()
    enableAdcReader()
end)
