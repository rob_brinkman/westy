node.setcpufreq(node.CPU80MHZ)

function loadConfiguration(configFile) 
    if file.open(configFile) then
        file.close() 
        dofile(configFile)
    else
        error(configFile .. " not found!!!!")
    end
end


function connectWifi (callback) 
    node.restore() 
    wifi.setmode(wifi.STATION)
    wifi.sta.config(wifi_ssid,wifi_password, 1) -- don't use auto connect we will control the connection
    wifi.sta.connect()

    tmr.alarm(1, 2000, 1, function() 
        status = wifi.sta.status()
        if (status ~= 5) then
            print("Waiting for WIFI to connect to " .. wifi_ssid ..", current status: " .. status)
        else
            print("Wifi connected: OK (" .. wifi.sta.getip() .. ")")
            tmr.stop(1)
            callback()
        end 
    end)
end

function connectMQTT(callback) 
    
    -- init mqtt client with keepalive timer 120sec
    m = mqtt.Client(mqtt_clientid, 30, nil, nil)
    
    -- register event handlers
    m:on("connect", function(client) 
        print ("MQTT connected: OK") 
        callback(client)
        tmr.stop(2)
    end)

    m:on("offline", function(client) 
        print ("MQTT disconnected, reconnecting")
        tmr.alarm(2, 5000, 1, function()
            connectMQTT(callback)
        end)
         
    end)

    m:on("message", function(client, topic, data) 
        if data ~= nil then
            print(data)
        end
    end)
       
    -- connect to broker
    m:connect(mqtt_host, mqtt_port)
end


fullLevel = 1
halfLevel = 2
emptyLevel = 5

function initPins()
    initPin(fullLevel)
    initPin(halfLevel)
    initPin(emptyLevel)      
end

function initPin(pin)
    gpio.mode(pin, gpio.INPUT)
    gpio.trig(pin, "both",  levelChanged)
        
end

function levelChanged() 
    local level = detectLevel()
    print("Level: " .. level)
    if (level) then
        tmr.alarm(3, 200, 0, function()
            m:publish("/sensor/water/clean/level", tostring(level), 0, 0, nil)
        end)
    end
end

function detectLevel() 
    if (gpio.read(fullLevel) == gpio.HIGH) then
        return 3
    end    
    if (gpio.read(halfLevel) == gpio.HIGH) then
        return 2
    end
    if (gpio.read(emptyLevel) == gpio.HIGH) then
        return 1
    end
    return 0
end

loadConfiguration("settings.lua")

connectWifi(function()
    connectMQTT(function()
        initPins()
        -- publish current level
        levelChanged()        
    end)
end)


