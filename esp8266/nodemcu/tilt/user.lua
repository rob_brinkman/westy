require("shared")

tilt1Pin = 7
tilt2Pin = 8
dhtPin = 6

function initTiltSensor()
    initTiltPin(tilt1Pin)
    initTiltPin(tilt2Pin)  
end

function initTiltPin(pin)
    gpio.mode(pin, gpio.INPUT)
    gpio.trig(pin, "both",  tiltChanged)
        
end

function tiltChanged() 
    local tilt = readTilt()
    if (tilt and mqttConnected) then
        print("Publish Tilt: " .. tilt)
        m:publish("/sensor/position/tilt", tilt, 0, 0, nil)    
    end
end

function readTilt() 
    local level1Value = gpio.read(tilt1Pin)
    local level2Value = gpio.read(tilt2Pin)
    local combinedValue = level1Value + (2*level2Value)
    
    if (combinedValue == 0) then
        return "front"
    end
    if (combinedValue == 1) then
        return "right"
    end   
    if (combinedValue == 2) then
        return "left"
    end
    if (combinedValue == 3) then
        return "back"
    end
    return "none"
end


function enableTemperatureReader() 
    tmr.alarm(3, 5000, 1, function() 
        status, temp, humi, temp_dec, humi_dec = dht.read11(dhtPin)
        if (status == dht.OK and mqttConnected) then  
            print("Published Temperature: " .. temp)
            m:publish("/sensor/climate/inside/temperature", temp, 0, 0, nil)
            print("Published Humidity: " .. humi)
            m:publish("/sensor/climate/inside/humidity", humi, 0, 0, nil)      
        end
    end)
end

loadConfiguration("settings.lua")

-- mute serial to improve stability
node.output(function() end, 0)

connectWifi(function()
    connectMQTT()
    enableTemperatureReader()
    initTiltSensor()       
end)


