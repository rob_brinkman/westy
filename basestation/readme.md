# Central

## Vertx

* Version 3.0.0-milestone6 because this is the latest version the mqtt module supports


## MQTT

* Added dependency to lib folder because it's not available on a public repo
* Simple broker configuration in `src/main/resources/config.json`
 
## Running
 
`> gradle run`

or

`> gradle fatJar`
`> java -jar build/libs/central-fat-0.0.1-SNAPSHOT.jar -conf src/main/resources/config.json` 

# ESP8266

## Different Module Types

http://l0l.org.uk/2014/12/esp8266-modules-hardware-guide-gotta-catch-em-all/

* Most shown board ESP-01 
  * lacks access to the needed GPIO ports
  * usb to serial cable needed to flash
  * additional pin wiring needed to flash
  * seems appropriate in a very simple setup where powering the device triggers a connection to wifi and some action (for instance calling a url or sending a mqtt message)
  
* Developer board based on ESP-12 is easy to use. 
  * Directly powered from USB
  * Breadboard compatible
  * Lot's of (GPIO) ports available
  * Comes preflashed with NodeMCU
  
* ESP-12 seems to be a nice alternative if you do want to create your own layouts, the size of this module does not fit my default breadboard ;(  

## NodeMCU

https://learn.adafruit.com/adafruit-huzzah-esp8266-breakout/using-nodemcu-lua
https://github.com/nodemcu/nodemcu-firmware/wiki/nodemcu_api_en#nodemcu-api-instruction

### Flashing

Eventually flashed using the Windows tool with Wine because of issues with the firmware after flashing with esptool (random reboots and filesystem issues).

http://blog.squix.ch/2015/05/esp8266-flashing-nodemcu-v10-on-mac-os.html

Download FLASH_DOWNLOAD_TOOLS_v1.2_150512.rar from http://bbs.espressif.com/viewtopic.php?f=5&t=433

I was most successful using the integer version of the latest firmware:
https://github.com/nodemcu/nodemcu-firmware/releases/tag/0.9.6-dev_20150704

## Schematics

* Power using USB
* Sensor GND to GND, VCC to VIN (5V), OUT to GPIO2 through a x resistor
* Led GND to GND, VCC to GPIO3 through a 1k Ohms 5% resistor (brown, black, red, gold)

## Lua

* Use the ESPlorer IDE: http://esp8266.ru/esplorer/

### Working setup
* upload init.lua on the device
* upload user.lua on the device
* compile user.lua to user.lc
* upload telnet.lua on the device
* compile telnet.lua to telnet.lc
* create and upload a file settings.lua on the device containing:
`  wifi_ssid = "ssid_name"
   wifi_password = "password"
   mqtt_clientid = "motion1"
   mqtt_host = "ip of host running vertx"
   mqtt_port = 8883
`

### Tips & Tricks
* Use telnet to access device from remote (telnet to port 2323 of the given IP)
* Even redirect a fake serial port to telnet to use the IDE: 

# Other

* https://thingspeak.com/