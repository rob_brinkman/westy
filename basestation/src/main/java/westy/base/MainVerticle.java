package westy.base;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MainVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start() throws Exception {

        logger.debug("Config: {}", config().toString());
        if (config().isEmpty()) {
            throw new IllegalArgumentException("Please pass a valid config file using -conf <configfile>");
        }

        deployVerticle("io.github.giovibal.mqtt.MQTTBroker", config().getJsonObject("mqtt") );
        deployVerticle("westy.base.mqtt.MqttEventBusBridge");
        deployVerticle("westy.base.persistence.RedisVerticle", config().getJsonObject("redis"));
        deployVerticle("westy.base.web.WebVerticle", config().getJsonObject("web"));

        deployVerticle("westy.base.simulation.SensorSimulationVerticle", config().getJsonObject("simulation"));
    }

    private void deployVerticle(String verticleName) {
        deployVerticle(verticleName, new JsonObject());
    }

    private void deployVerticle(String verticleName, JsonObject verticleConfig) {
        if (verticleConfig == null) {
            throw new IllegalStateException("Could not deploy verticle: " + verticleName + ", configuration not found");
        }

        vertx.deployVerticle(verticleName, new DeploymentOptions().setConfig(verticleConfig), deploymentResult -> {
            if (deploymentResult.succeeded()) {
                logger.info("Deployed Verticle: {}", verticleName);
            } else {
                logger.error("Could not deploy Verticle: {}, reason: {}", verticleName, deploymentResult.cause().getMessage(), deploymentResult.cause());
            }
        });
    }

}
