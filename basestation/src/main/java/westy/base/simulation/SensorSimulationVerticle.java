package westy.base.simulation;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SensorSimulationVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(SensorSimulationVerticle.class);

    private static final long DEFAULT_RAMP_UP_DELAY = 2000l;

    @Override
    public void start() throws Exception {
        if (config().getBoolean("enabled")) {

            JsonArray simulators = config().getJsonArray("simulators");

            long rampUpDelay = config().getLong("ramp_up_delay", DEFAULT_RAMP_UP_DELAY);
            logger.info("Simulation is enabled, starting each one after  {} ms", rampUpDelay);

            for (Object simulatorObj : simulators) {
                if (simulatorObj instanceof JsonObject) {
                    vertx.setTimer(rampUpDelay += rampUpDelay, handler ->
                                    launchSimulator(config().getString("mqttUrl"), (JsonObject) simulatorObj)
                    );
                }
            }
        } else {
            logger.info("Simulation is disabled");
        }
    }

    private void launchSimulator(String mqttUrl, JsonObject simulatorConfig) {
        String clientId = simulatorConfig.getString("clientId");

        logger.info("Creating simulator: {}, with config: {}", clientId, simulatorConfig.toString());

        SensorSimulator simulator = new SensorSimulator(mqttUrl, simulatorConfig);

        vertx.setPeriodic(randomizeDelay(simulator.getDelay()), handler -> simulator.simulate());
    }

    private Integer randomizeDelay(int delay) {
        Double twentyPct = delay * 20 / 100d;

        Double randomizeDelay = delay - (twentyPct / 2) + (Math.random() * twentyPct);
        logger.info("Delay: {}, random: {}", delay, randomizeDelay);
        return randomizeDelay.intValue();
    }
}

