package westy.base.simulation;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import westy.base.simulation.payload.PayloadGeneratorFactory;

/**
 * Created by rb on 12/10/15.
 */
class SensorSimulator {

    private final Logger logger = LoggerFactory.getLogger(SensorSimulator.class);

    private final JsonObject config;

    private final String mqttUrl;

    private MqttClient mqttClient;

    public SensorSimulator(String mqttUrl, JsonObject config) {
        this.mqttUrl = mqttUrl;
        this.config = config;
    }

    public Integer getDelay() {
        return config.getInteger("delay");
    }

    public String getClientId() {
        return config.getString("clientId");
    }

    public void simulate() {
        if (mqttClient == null || !mqttClient.isConnected()) {
            connectMqttClient();
        }

        for (Object topicObj : config.getJsonArray("topics")) {
            simulateTopic((JsonObject) topicObj);
        }
    }

    private void simulateTopic(JsonObject topic) {
        String topicId = topic.getString("topic");
        String payload = generatePayLoad(topicId, topic.getJsonArray("payload"));

        try {
            mqttClient.publish(topicId, payload.getBytes(), 0, false);
            logger.info("Simulate: {}, withPayLoad: {}", topicId, payload);
        } catch (MqttException e) {
            logger.error("Could not send mqtt message to: {}, reason: {}", topicId, e.getMessage(), e.getCause());
        }
    }

    private String generatePayLoad(String topicId, JsonArray payload) {
        return generateValue(topicId, payload.getJsonObject(0)).toString();
    }

    private Object generateValue(String topicId, JsonObject payloadItem) {
        return PayloadGeneratorFactory.getInstance(payloadItem).generateValue(topicId, payloadItem);
    }

    private void connectMqttClient() {
        try {
            mqttClient = new MqttClient(this.mqttUrl, getClientId());
            mqttClient.connectWithResult(new MqttConnectOptions());
            logger.info("MQTT Connected: {}", mqttClient.isConnected());
        } catch (MqttException e) {
            logger.error("Could not connect to MQTT, reason: {}", e.getMessage(), e.getCause());
        }
    }
}
