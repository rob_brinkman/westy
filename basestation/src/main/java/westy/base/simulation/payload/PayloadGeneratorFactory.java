package westy.base.simulation.payload;

import io.vertx.core.json.JsonObject;

/**
 * Created by rb on 12/10/15.
 */
public class PayloadGeneratorFactory {

    private final static NumericPayloadGenerator numericPayloadGenerator = new NumericPayloadGenerator();
    private final static PayloadGenerator selectPayloadGenerator = new SelectPayloadGenerator();

    public static PayloadGenerator getInstance(JsonObject payLoadItem) {
        switch (payLoadItem.getString("type", "numeric")) {
            case "numeric":
                return numericPayloadGenerator;
            case "select":
                return selectPayloadGenerator;
            default:
                throw new IllegalArgumentException("No valid payloadGenerator for config: " + payLoadItem.toString());
        }

    }
}
