package westy.base.simulation.payload;

import io.vertx.core.json.JsonObject;

/**
 * Created by rb on 12/10/15.
 */
abstract public class StatefulPayloadGenerator implements PayloadGenerator {

    final JsonObject state;

    public StatefulPayloadGenerator() {
        state = new JsonObject();
    }

}
