package westy.base.simulation.payload;

import io.vertx.core.json.JsonObject;

/**
 * Created by rb on 12/10/15.
 */
public interface PayloadGenerator {

    Object generateValue(String topicId, JsonObject payLoadItem);
}
