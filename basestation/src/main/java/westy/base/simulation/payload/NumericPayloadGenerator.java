package westy.base.simulation.payload;

import io.vertx.core.json.JsonObject;

import java.util.Random;

/**
 * Created by rb on 12/10/15.
 */
public class NumericPayloadGenerator extends StatefulPayloadGenerator {

    private final Random random = new Random();

    @Override
    public Object generateValue(String topicId, JsonObject payLoadItem) {
        Object result;

        String name = payLoadItem.getString("name");
        String stateKey = topicId + "|" + name;

        Double minValue = payLoadItem.getDouble("minValue");
        Double maxValue = payLoadItem.getDouble("maxValue");

        Double maxDeltaWithCurrent = (maxValue - minValue) / payLoadItem.getDouble("maxDeltaWithCurrent", 10.0);
        Double currentValue = state.getDouble(stateKey, (minValue + maxValue) / 2);

        Double minRand = currentValue - maxDeltaWithCurrent;
        if (minRand < minValue) minRand = minValue;

        Double maxRand = currentValue + maxDeltaWithCurrent;
        if (maxRand > maxValue) maxRand = maxValue;

        result = minRand + (maxRand - minRand) * random.nextDouble();

        state.put(stateKey, result);

        if (shouldRoundToInteger(payLoadItem)) {
            result = Math.round((Double) result);
        }
        return result;
    }

    private boolean shouldRoundToInteger(JsonObject payLoadItem) {
        return !payLoadItem.getValue("maxValue").toString().contains(".");
    }
}
