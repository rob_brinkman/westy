package westy.base.simulation.payload;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Random;

/**
 * Created by rb on 12/10/15.
 */
public class SelectPayloadGenerator implements PayloadGenerator {

    private final Random random = new Random();

    @Override
    public Object generateValue(String topicId, JsonObject payLoadItem) {
        JsonArray values = payLoadItem.getJsonArray("values");
        return values.getValue(random.nextInt(values.size()));
    }
}
