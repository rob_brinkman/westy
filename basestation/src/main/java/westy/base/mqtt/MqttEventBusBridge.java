package westy.base.mqtt;

import io.github.giovibal.mqtt.parser.MQTTDecoder;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import org.dna.mqtt.moquette.proto.messages.PublishMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.NumberFormat;
import java.text.ParseException;

public class MqttEventBusBridge extends AbstractVerticle {

    private static final String EVENTBUS_MQTT_PREFIX = "westy.base.mqtt";
    private static final String PAYLOAD_KEY = "payload";

    private final Logger logger = LoggerFactory.getLogger(MqttEventBusBridge.class);

    private final MQTTDecoder mqttDecoder = new MQTTDecoder();

    @Override
    public void start() throws Exception {

        vertx.eventBus().consumer("io.github.giovibal.mqtt", event -> {
            try {
                PublishMessage mqttMessage = (PublishMessage) mqttDecoder.dec((Buffer) event.body());

                String topic = mqttMessage.getTopicName();

                JsonObject eventBusMessage = new JsonObject();
                eventBusMessage.put("topic", topic);
                eventBusMessage.put("received", System.currentTimeMillis());

                addTypedPayload(eventBusMessage, mqttMessage.getPayloadAsString());

                publishMessage(eventBusMessage, getAddressForTopic(topic));
                publishMessage(eventBusMessage, EVENTBUS_MQTT_PREFIX + ".all");

            } catch (Exception e) {
                logger.error("Could not decode MQTT Message, reason: {}", e.getMessage(), e);
            }
        });
    }

    private void addTypedPayload(JsonObject eventBusMessage, String payloadAsString) {
        try {
            eventBusMessage.put(PAYLOAD_KEY, NumberFormat.getInstance().parse(payloadAsString.trim()));
        } catch (ParseException e) {
            eventBusMessage.put(PAYLOAD_KEY, payloadAsString.trim());
        }
    }

    private void publishMessage(JsonObject eventBusMessage, String addressForTopic) {
        logger.debug("Publishing {} to {}", eventBusMessage, addressForTopic);
        vertx.eventBus().publish(addressForTopic, eventBusMessage);
    }

    private String getAddressForTopic(String topic) {
        return EVENTBUS_MQTT_PREFIX + "." + topic.replace('/', '.').substring(1);
    }
}
