package westy.base.persistence;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.redis.RedisClient;
import io.vertx.redis.op.RangeLimitOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

/**
 * Created by rb on 14/09/15.
 */
public class RedisVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(RedisVerticle.class);

    private RedisClient redis;

    @Override
    public void start() throws Exception {
        redis = RedisClient.create(vertx, config());
        vertx.eventBus().consumer("westy.base.mqtt.all", event -> {

            JsonObject body = (JsonObject) event.body();

            String topic = body.getString("topic");

            addToHash(getKeyForTopic(topic), body);
            incrementHashField(getKeyForTopic(topic), "count");

            addToSortedSet("sensors", body.getDouble("received"), getKeyForTopic(topic));
            addToSortedSet(getKeyForTopic(topic, "log"), body.getDouble("received"), body);
        });

        vertx.eventBus().consumer("westy.base.mqtt.request", event -> {
            JsonObject body = (JsonObject) event.body();

            String topic = body.getString("topic");
            Long history = body.getLong("history", 60l);

            JsonObject result = new JsonObject();

            long now = System.currentTimeMillis();
            String min = String.valueOf(now - (history*1000));
            String max = String.valueOf(now);

            redis.zrangebyscore(getKeyForTopic(topic, "log"), min, max, RangeLimitOptions.NONE, redisResult -> {
                result.put("topic", topic);
                if (redisResult.succeeded()) {
                    result.put("result", redisResult.result().stream().map(json -> new JsonObject((String) json)).collect(Collectors.toList()));
                } else {
                    result.put("error", redisResult.cause().getMessage());
                }
                event.reply(result);
            });


        });
    }

    private void incrementHashField(String key, String field) {
        redis.hincrby(key, field, 1, result -> {
            if (result.failed()) {
                logger.error("Could not increment field: {} from hash: {}", field, key, result.cause().getMessage(), result.cause());
            } else {
                logger.debug("Incremented field: {} from hash: {} to: {}", field, key, result.result());
            }
        });
    }

    private void addToSet(String key, String value) {
        redis.sadd(key, value, result -> {
            if (result.failed()) {
                logger.error("Could not add item to set: {), reason: {}", key, result.cause().getMessage(), result.cause());
            } else {
                logger.debug("Added item to storted set: {}", key);
            }
        });
    }

    private void addToSortedSet(String key, Double score, Object value) {
        redis.zadd(key, score, value.toString(), result -> {
            if (result.failed()) {
                logger.error("Could not add item to sorted set: {) and score: {}, reason: {}", key, score, result.cause().getMessage(), result.cause());
            } else {
                logger.debug("Added item to storted set: {} and score: {}", key, score);
            }
        });
    }

    private void addToHash(String key, JsonObject body) {
        body.forEach(entry -> {
            redis.hset(key, entry.getKey(), entry.getValue().toString(), result -> {
                if (result.failed()) {
                    logger.error("Could not set item for hash: {} with key: {}, reason: {}",
                            key,
                            entry.getKey(),
                            result.cause().getMessage(),
                            result.cause()
                    );
                } else {
                    logger.debug("Set item for hash: {} with key: {}", key, entry.getKey());
                }
            });
            // If the entry is a JsonObject add it's values directly as value to the hash
            if (entry.getValue() instanceof JsonObject) {
                addToHash(key, (JsonObject) entry.getValue());
            }
        });
    }

    private String getKeyForTopic(String topic, String postfix) {
        return getKeyForTopic(topic) + ":" + postfix;
    }

    private String getKeyForTopic(String topic) {
        return topic.replace("/", ":").substring(1);
    }
}
