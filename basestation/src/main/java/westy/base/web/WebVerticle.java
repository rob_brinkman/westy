package westy.base.web;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;

/**
 * Created by rb on 12/10/15.
 */
public class WebVerticle extends AbstractVerticle {

    /*
    https://github.com/vert-x3/vertx-web/blob/master/vertx-web/src/main/asciidoc/java/index.adoc
     */

    @Override
    public void start() throws Exception {

        HttpServer server = vertx.createHttpServer();


        SockJSHandler sockJSHandler = SockJSHandler.create(vertx);

        PermittedOptions outboundPermitted = new PermittedOptions().setAddressRegex("westy.base.mqtt.*");
        PermittedOptions inboundPermitted = new PermittedOptions().setAddress("westy.base.mqtt.request");
        BridgeOptions options = new BridgeOptions().addOutboundPermitted(outboundPermitted).addInboundPermitted(inboundPermitted);

        sockJSHandler.bridge(options);

        Router router = Router.router(vertx);
        router.route("/eventbus/*").handler(sockJSHandler);
        router.route().handler(StaticHandler.create());


        server.requestHandler(router::accept).listen(config().getInteger("port"));
    }
}
