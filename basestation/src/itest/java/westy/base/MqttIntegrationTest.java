package westy.base;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.embedded.RedisServer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by rb on 12/10/15.
 */
public class MqttIntegrationTest {

    private final Logger logger = LoggerFactory.getLogger(MqttClientIntegrationTest.class);

    private final Integer mqttPort = 8801;
    private final Integer redisPort = 8901;

    private Vertx vertx;
    private RedisServer redisServer;

    private MqttClient mqttClient;

    private List<MessageConsumer> consumers = new ArrayList<>();

    @Before
    public void setup() throws MqttException, InterruptedException {
        startEmbeddedRedis();
        startEmbeddedVertx();
        connectMqttClient();
    }

    @After
    public void cleanup() throws MqttException, InterruptedException {
        disconnectMqttClient();
        consumers.forEach(consumer -> {
            consumer.unregister();
        });
        stopEmbeddedRedis();
        stopEmbeddedVertx();
    }

    void registerVertxConsumer(String address, Handler<Message<Object>> assertMessageHandler) {
        MessageConsumer consumer = vertx.eventBus().consumer(address, assertMessageHandler);
        consumers.add(consumer);
    }

    void publishMqttMessage(String topic, String payload) {
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setPayload(payload.getBytes());
        try {
            mqttClient.publish(topic, mqttMessage);
        } catch (MqttException e) {
            throw new IllegalStateException(e);
        }
    }

    private void connectMqttClient() throws MqttException {
        mqttClient = new MqttClient("tcp://localhost:" + mqttPort, "myClientId");
        mqttClient.connectWithResult(new MqttConnectOptions());

        logger.info("MQTT Connected: {}", mqttClient.isConnected());
    }

    private void disconnectMqttClient() throws MqttException {
        if (mqttClient.isConnected()) {
            mqttClient.disconnect();
        }
    }

    private void startEmbeddedVertx() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        vertx = Vertx.vertx();

        vertx.deployVerticle(MainVerticle.class.getName(), getDeploymentOptions(), event -> {
            if (event.succeeded()) {
                countDownLatch.countDown();
            } else {
                logger.error("Could not deploy MainVerticle: {}", event.cause().getMessage(), event.cause());
            }
        });

        countDownLatch.await(MqttClientIntegrationTest.COUNT_DOWN_LATCH_TIMEOUT_SECONDS, TimeUnit.SECONDS);

        if (countDownLatch.getCount() > 0) {
            throw new IllegalStateException("Verticle not deployed successfully");
        }

        Thread.sleep(500);

    }

    private void stopEmbeddedVertx() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        vertx.close(event -> {
            countDownLatch.countDown();
        });
        countDownLatch.await(MqttClientIntegrationTest.COUNT_DOWN_LATCH_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    private void startEmbeddedRedis() {
        redisServer = RedisServer.builder().port(redisPort).setting("bind 127.0.0.1").build();
        redisServer.start();
    }

    private void stopEmbeddedRedis() {
        if (redisServer.isActive()) {
            redisServer.stop();
        }
    }

    private DeploymentOptions getDeploymentOptions() {
        DeploymentOptions options = new DeploymentOptions();
        JsonObject config = new JsonObject();
        config.put("mqtt", new JsonObject().
                put("brokers", new JsonArray().
                        add(new JsonObject().
                                put("tcp_port", mqttPort))));
        config.put("redis", new JsonObject().
                put("host", "localhost").
                put("port", redisPort));
        config.put("simulation", new JsonObject().
                put("enabled", false));
        config.put("web", new JsonObject().
                put("port", 8080));
        options.setConfig(config);
        return options;
    }
}
