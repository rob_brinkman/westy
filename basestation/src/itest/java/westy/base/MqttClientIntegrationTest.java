package westy.base;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by rb on 14/09/15.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MqttClientIntegrationTest extends MqttIntegrationTest {

    private final Logger logger = LoggerFactory.getLogger(MqttClientIntegrationTest.class);

    public static final int COUNT_DOWN_LATCH_TIMEOUT_SECONDS = 3;



    @Test
    public void testMessageWithInteger() throws MqttException, InterruptedException {

        CountDownLatch countDownLatch = new CountDownLatch(2);

        Handler<Message<Object>> assertMessageHandler = event -> {
            JsonObject body = (JsonObject) event.body();
            assertThat(body.getString("topic"), equalTo("/a/b/test"));


            logger.info("Body: {}", body.toString());

            assertThat(body.getInteger("payload"), equalTo(1));
            countDownLatch.countDown();
        };

        registerVertxConsumer("westy.base.mqtt.a.b.test", assertMessageHandler);
        registerVertxConsumer("westy.base.mqtt.all", assertMessageHandler);

        publishMqttMessage("/a/b/test", "1");

        countDownLatch.await(COUNT_DOWN_LATCH_TIMEOUT_SECONDS, TimeUnit.SECONDS);

        if (countDownLatch.getCount() > 0) {
            fail("Not all exepected events occurred, " + countDownLatch.getCount() + " left");
        }
    }

    @Test
    public void testMessageWithString() throws MqttException, InterruptedException {

        CountDownLatch countDownLatch = new CountDownLatch(2);

        Handler<Message<Object>> assertMessageHandler = event -> {
            JsonObject body = (JsonObject) event.body();
            assertThat(body.getString("topic"), equalTo("/a/b/test"));


            logger.info("Body: {}", body.toString());

            assertThat(body.getString("payload"), equalTo("string"));
            countDownLatch.countDown();
        };

        registerVertxConsumer("westy.base.mqtt.a.b.test", assertMessageHandler);
        registerVertxConsumer("westy.base.mqtt.all", assertMessageHandler);

        publishMqttMessage("/a/b/test", "string");

        countDownLatch.await(COUNT_DOWN_LATCH_TIMEOUT_SECONDS, TimeUnit.SECONDS);

        if (countDownLatch.getCount() > 0) {
            fail("Not all exepected events occurred, " + countDownLatch.getCount() + " left");
        }
    }
}
